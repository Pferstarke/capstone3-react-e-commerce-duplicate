import {useEffect, useContext, useState} from 'react';
import UserContext from '../UserContext';
import { Table, Button, Container} from 'react-bootstrap';
import Swal from "sweetalert2";
import { mdiDatabaseEdit, mdiKeyboardReturn, mdiDelete } from '@mdi/js';
import Icon from '@mdi/react';
import BackButton from '../components/BackButton';
import {useNavigate} from 'react-router-dom';

export default function UserRole (){
		const {user} = useContext(UserContext);
		const [profile, setProfile] = useState([]);
		console.log(user)


		const setAdmin = (userId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`,{
				method: 'PATCH',
				headers: {
					'Content-Type': 'applications/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(data => {
				console.log(data.isAdmin)
				return data.isAdmin;
			})

			window.location.reload();			
		}

		const setGuest = (userId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsGuest`,{
				method: 'PATCH',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(data => {
				console.log(data.isAdmin)
				return data.isAdmin;
			})

			window.location.reload();
		}


		const deleteUser = (userId) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/delete`,{
				method: 'DELETE',
				headers: {
					'Content-Type': 'applications/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(data => {
				console.log(data.isAdmin)
				// navigate('/products');
				return data.isAdmin;
			})
			// if(userId){
			// 		Swal.fire({
			// 			title: 'User deleted',
			// 			icon: 'info',
			// 			text: `User ID: ${userId}`
			// 		})
			// }
			// Swal.fire({
			//   position: 'top-end',
			//   icon: 'success',
			//   title: 'Your work has been saved',
			//   showConfirmButton: false,
			//   timer: 1500
			// })
			.then(window.location.reload());			
		}


		useEffect(() =>{
			fetch(`${process.env.REACT_APP_API_URL}/users/getAllUsers`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(data => {
				console.log(data);
				setProfile(data.map(profile=> {
					console.log(profile.isAdmin)
					return(
						<tr key={profile._id}>
							<td>{profile._id}</td>
							<td>{profile.firstName}</td>
							<td>{profile.lastName}</td>
							<td>{profile.email}</td>
							<td>{profile.isAdmin ? "Admin" : "Guest"}</td>
							<td>
								{(profile.isAdmin?
									(
										<>	
											{/*<Button onClick={() =>setAdmin(profile._id)}>Set as Admin</Button>*/}
											<Button className="m-1" variant="success" onClick={() =>setGuest(profile._id)}><Icon path={mdiDatabaseEdit  } size={1}/></Button>
											<Button className="m-1" variant="danger" onClick={() =>deleteUser(profile._id)}><Icon path={mdiDelete} size={1}/></Button>
										</>
									)
								:
									(
										<>
											<Button className="m-1" onClick={() =>setAdmin(profile._id)}><Icon path={mdiKeyboardReturn} size={1}/></Button>
											<Button className="m-1" variant="danger" onClick={() =>deleteUser(profile._id)}><Icon path={mdiDelete} size={1}/></Button>
											{/*<Button variant="success" onClick={() =>setGuest(profile._id)}>Set as User</Button>*/}
										</>
									))
								}
							</td>
						</tr>
					)
				}))
			})

		}, [])

		return(
			<>
			<Container className="table-responsive">
			<h1 className="text-center mt-5 mb-3">Users</h1>
			<Table className="text-center"  striped bordered hover>
				<thead>
					<tr>
						<th>User ID</th>
						<th>First name</th>
						<th>Last name</th>
						<th>Email</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				{profile}
				</tbody>
			</Table>
			<BackButton/>
			</Container>
			</>
		)


}