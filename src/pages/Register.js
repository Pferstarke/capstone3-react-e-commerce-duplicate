import {Form, Button, Stack} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
// import Logout from '../pages/Logout'
import Swal from 'sweetalert2';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

export default function Register(){

		const {user} = useContext(UserContext);
		console.log(user);
		
		const navigate = useNavigate();

		// State hooks to store the values of input fields
		const [firstname, setFirstname] = useState('');
		const [lastname, setLastname] = useState('');
		const [mobileNumber, setMobileNumber] = useState('');
		const [email, setEmail] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		// State to determine whether submit button is enabled or not
		const [isActive,setIsActive] = useState(false);


		function registerUser(event){
			// Prevents page redirection via form submission
			event.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					firstName: firstname,
					lastName: lastname,
					mobileNumber: mobileNumber,
					email: email,
					password: password1
				})

			})
			.then(result => result.json())
			.then(data => {
				console.log('Registration data');
				console.log(data);

				if(data){
					Swal.fire({
						title: 'Successful registration',
						icon: 'success',
						text: 'You have successfully registered.'
					})

					navigate('/login')
				}
				else{
					Swal.fire({
						title: 'Email address is already used.',
						icon: 'error',
						text: 'Please different email address.'
					})
				}
			})
			.catch((error) => console.error(error));

			// Clear input fields
			setFirstname('');
			setLastname('');
			setMobileNumber('');

			setEmail('');
			setPassword1('');
			setPassword2('');
			// alert('Thank you for registering');
		}
		useEffect(() => {
			if((email !== '' && password1 !=='' && password2 !== '') && (password1 === password2)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [firstname, lastname, mobileNumber, email, password1, password2])
		
		return(
			(user.token !== null)
			?
			<Navigate to="/errr" />
			:
			<Stack className="col-md-5 mt-5 pt-5 mx-auto">
				<Form onSubmit={event => registerUser(event)}>
					<h3>Register</h3>
		            <Form.Group controlId="firstname">
		                <Form.Label>First name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="First name"
			                value = {firstname}
			                onChange= {event => setFirstname(event.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="lastname">
		                <Form.Label>Last name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Last name"
			                value = {lastname}
			                onChange= {event => setLastname(event.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="mobileNumber">
		                <Form.Label>Mobile Number</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Mobile Number"
			                value = {mobileNumber}
			                onChange= {event => setMobileNumber(event.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value = {email}
			                onChange = {event => setEmail(event.target.value)}
			                required
		                />
		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value = {password1}
			                onChange= {event => setPassword1(event.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                value = {password2}
			                onChange = {event => setPassword2(event.target.value)}
			                required
		                />
		            </Form.Group>
		            {isActive ? //true
			            <Button variant="primary" type="submit" id="submitBtn">
			            	Submit
			            </Button>
		            	: //false
		            	<Button variant="primary" type="submit" id="submitBtn" disabled>
		            		Submit
		            	</Button>

		        	}

		        </Form>
	        </Stack>
		)
}