import {Fragment} from 'react';
import {Link} from 'react-router-dom';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';

export default function Error(){

	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link as={Link} to="/">homepage</Link>.</p>
		</Fragment>
	)
}