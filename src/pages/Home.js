import {Link, BrowserRouter as Router, useLocation} from 'react-router-dom';
import {Container, Row, Col, Button} from 'react-bootstrap';
import './Home.css';
import elec from '../images/elec.jpg';
import UserContext from '../UserContext';
import {useContext} from 'react';
import Products from './Products';

export default function Home(){
	const {user} = useContext(UserContext);
	console.log(user);


	if(user.isAdmin){
		return(
			<Container id="home-container" className="pt-5 mt-5">
			<Row>
				<Col md>
					<div className="" id="div-banner">
						<h1 id="home-h1" className="text-center mt-5 pt-3">Best Deals are here!</h1>
					</div>
				</Col>
				<Col className="sm-mt-5 sm-pt-5">
					<img src={elec} className="img-fluid mt-5 pt-3"/>
				</Col>
			</Row>
			</Container>
		)
	}
	if(user.isAdmin == false){
		return(
			<Container id="home-container" className="pt-5 mt-5">
			<Row>
				<Col md>
					<div className="" id="div-banner">
						<h1 id="home-h1" className="text-center mt-5 pt-3">Best Deals are here!</h1>
					</div>
					<Col className="text-center">
						<Button as={Link} to="/products" style={{fontSize: '50px', margin: '10px', padding: '20px'}}>Shop now</Button>
					</Col>
				</Col>
					<img src={elec} className="img-fluid mt-5 pt-3"/>
				
			</Row>
			</Container>
		)
	}
	if(user.token == null){
		return(
			<Container id="home-container" className="pt-3 mt-3">
			<Row>
				<Col md>
					<div className="" id="div-banner">
						<h1 id="home-h1" className="text-center mt-5 pt-3">Best Deals are here!</h1>
					</div>
					<Row className="text-center">
						<Button as={Link} to="/login" style={{fontSize: '20px'}}>Shop now</Button>
					</Row>
				</Col>
				<Col className="sm-mt-5 sm-pt-5">
					<img src={elec} className="img-fluid mt-5 pt-3"/>
				</Col>
			</Row>
			</Container>
		)
	}
}