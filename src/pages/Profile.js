import AppNavbar from '../components/AppNavbar';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Card, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Profile.css';

export default function Profile (){
	const {user, setUser} = useContext(UserContext);
	console.log(user)
	const [userId, setUserId] = useState('');

	const [firstName, setfirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	
	const [totalAmount, setTotalAmount] = useState(0);
	const [purchasedOn, setPurchasedOn] = useState('');
	const [products, setProducts] = useState([]);

	const [orders, setOrders] = useState([]);


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				'Content-Type': 'applications/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setfirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setPassword(data.password);
			setMobileNumber(data.mobileNumber);
			console.log(data)

			setOrders(
				fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/getUserOrders`)
			)

		})

	}, [])
	console.log(mobileNumber)
	console.log(totalAmount);
	return(
		
		<div className="col-md-5 mt-5 pt-5 mx-auto" id="bg-card">
			<h2 className="mb-5 mx-auto">{firstName}'s Profile</h2>
				<h5>First name:</h5>
				<Card.Text className="">{firstName}</Card.Text>
				<h5>Last name:</h5>
				<Card.Text className="">{lastName}</Card.Text>
				<h5>Email:</h5>
				<Card.Text className="">{email}</Card.Text>
				<h5>Password:</h5>
				<Card.Text className="">{password}</Card.Text>
				<h5>Mobile number:</h5>
				<Card.Text className="">{mobileNumber}</Card.Text>
			{/*<Button as={Link} to="/dashboard/products/orders">Your Orders</Button>*/}
		</div>
		
	)
}