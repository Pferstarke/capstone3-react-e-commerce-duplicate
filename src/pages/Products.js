// import coursesData from '../data/coursesData';
import { Navigate, useParams, Link } from 'react-router-dom';
// import ProductCard from '../components/ProductCard';
import { Fragment, useEffect, useState, useContext } from 'react';

import UserContext from '../UserContext';
import ProductList from '../components/ProductList';

import {Button, Form, InputGroup, Col, Row, Container} from 'react-bootstrap'
import Icon from '@mdi/react';
import { mdiMagnify } from '@mdi/js';
import SearchProduct from './SearchProduct';

import {BrowserRouter as Router, useLocation} from 'react-router-dom';


export default function Products(){
	
	/*
		console.log("Contents of coursesData");
		console.log(coursesData);
		console.log(coursesData[0]);
	*/

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]); // in array yung course
	// const {productId} = useParams();
	// const [description, setDescription] = useState('');
	const [name, setName] = useState('');

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductList key={product._id} productProp={product} />
					
				)
			}))
		})
	}, []);

	

	return (
		// (user.isAdmin)?
		// 	<Navigate to="/admin" />
		// :
		<>
			<Container>
				<h1 className="text-center mt-5"> Products </h1>
				{/*<SearchProduct />*/}
				<Col md={{offset: 10 }}>
				<Button type="submit"as={Link} to="/products/find" className="col-md m-3"><Icon path={mdiMagnify} size={1}/></Button>
				</Col>
				{products}
			</Container>
			
		</>
	)
}