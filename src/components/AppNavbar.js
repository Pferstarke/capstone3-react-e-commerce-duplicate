import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import {Navbar, Button, Col} from 'react-bootstrap';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {useState, Fragment, useContext} from 'react';
import UserContext from '../UserContext';
import { Link, NavLink } from 'react-router-dom';
import Logout from '../pages/Logout';
import Icon from '@mdi/react';
import { mdiAccount, mdiHomeOutline, mdiShopping, mdiMagnify, mdiViewDashboardEdit } from '@mdi/js';
import Profile from '../pages/Profile';
import SearchProduct from '../pages/SearchProduct';


import './NavStyle.css';


export default function AppNavbar() {

  const {user, unsetUser} = useContext(UserContext);
  console.log(user);

  const [userId, SetUserId] = useState(localStorage.getItem('userId'));


  return (
    <Navbar expand="lg" id="nav-bg" className="mb-5 p-3">
      <Container>
        <Navbar.Brand className="text-white" style={{fontSize: '30px'}}>ME-Commerce</Navbar.Brand>
        <Button type="submit"as={Link} to="/products/find"><Icon path={mdiMagnify} size={1}/></Button>

        <Navbar.Toggle aria-controls="basic-navbar-nav"  />

        <Navbar.Collapse id="basic-navbar-nav" >

          <Nav className="ms-auto">
            {(user.isAdmin)?
            <></>
            :
            <Nav.Link className="mx-5" as={NavLink} to="/"><Icon path={mdiHomeOutline} size={1.5} style={{color: 'white'}} /></Nav.Link>
            }
            {(user.isAdmin)?
              <>
              <Nav.Link className="mx-5" as={NavLink} to="/login" style={{color: 'white'}}><Icon path={mdiViewDashboardEdit} size={1.5}/></Nav.Link>
              {/*<Nav.Link className="mx-5" as={NavLink} to="/products"><Icon path={mdiShopping} size={1.5}  style={{color: 'white'}}/></Nav.Link>*/}
              </>
              :
              <Nav.Link className="mx-5" as={NavLink} to="/products"><Icon path={mdiShopping} size={1.5}  style={{color: 'white'}}/></Nav.Link>
            }
            {(user.token !== null)?
            <NavDropdown className="mx-5" title=<Icon path={mdiAccount} size={1.5} style={{color: 'white'}}/> id="basic-nav-dropdown">
              <NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>
              {(user.isAdmin)
              ?
              <></>
              :
              <NavDropdown.Item as={Link} to="/dashboard/products/orders">View Orders</NavDropdown.Item>
              }
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={() => unsetUser()} as={NavLink} to="/logout" >
                Logout
              </NavDropdown.Item>
            </NavDropdown>
                  :
                  <Fragment>
                    <Nav.Link className="mx-5 text-light" as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link className="mx-5 text-light" as={NavLink} to="/register">Register</Nav.Link>
                  </Fragment>
              } 
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

