import {Card, Button, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {PropTypes} from 'prop-types';
import Swal from 'sweetalert2';
import {Link, NavLink} from 'react-router-dom';

export default function ProductList({productProp}) {

	// console.log("Contents of props: ");
	// console.log(props);
	// console.log(typeof props);
	const {user} = useContext(UserContext);
	console.log(user);
	const {_id, name, description, stocks, price, quantity} = productProp;

	// State Hooks (useState) - a way to store information within a component and track this information
		// getter, setter
		// variable, function to change the value of a variable
	console.log(productProp);

	if(user.isAdmin){
		return (
		    <Card className="m-3">
		        <Card.Body>
		            <h1 className="mb-5">{name}</h1>
		            <Card.Subtitle>Description:</Card.Subtitle>
		            <Card.Text>{description}</Card.Text>
		            <Card.Subtitle>Price:</Card.Subtitle>
		            <Card.Text>Php {price}</Card.Text>
		            <Card.Subtitle>Stocks:</Card.Subtitle>
		            <Card.Text>{stocks}</Card.Text>
		        </Card.Body>
		    </Card>
		)
	}
	if(user.isAdmin == false){
		return (
		    <Card className="col-md-10 m-3 mx-auto">
		        <Card.Body>
		        	<Row>
			        	<Col >
			        		<Card.Title className="text-center">{name}</Card.Title>
			        	</Col>
				        <Button className="mb-2 mt-2" as={Link} to={`/products/${_id}`}>Checkout</Button>
				        {/*<Button variant="secondary" className="mb-4" as={Link} to={`/products/${_id}`} disabled>Add to cart</Button>*/}
		        	</Row> 
		        	<Row>
			            <Col md={{offset: 2}}>
			            	<Card.Subtitle>Description:</Card.Subtitle>
			            	<Card.Text>{description}</Card.Text>
			            </Col>
			            <Col>
				            <Card.Subtitle>Price:</Card.Subtitle>
				            <Card.Text>Php {price}</Card.Text>
			            </Col>
			            <Col>
				            <Card.Subtitle>Stocks:</Card.Subtitle>
				            <Card.Text>{stocks}</Card.Text>
			            </Col>
		            </Row>

		        </Card.Body>
		    </Card>
		)
	}
	if(user.token == null){
		return (
		    <Card className="col-md-10 m-3 mx-auto">
		        <Card.Body>
		        	<Row>
        	        	<Col >
        	        		<Card.Title className="text-center">{name}</Card.Title>
        	        	</Col>
        		        <Button className="mb-2 mt-2" as={Link} to={`/login`}>Login first</Button>
        		        <Button variant="secondary" className="mb-4" as={Link} to={`/products/${_id}`} disabled>Add to cart</Button>
		        	</Row> 
		        	<Row>
			            <Col md={{offset: 2}}>
			            	<Card.Subtitle>Description:</Card.Subtitle>
			            	<Card.Text>{description}</Card.Text>
			            </Col>
			            <Col>
				            <Card.Subtitle>Price:</Card.Subtitle>
				            <Card.Text>Php {price}</Card.Text>
			            </Col>
			            <Col>
				            <Card.Subtitle>Stocks:</Card.Subtitle>
				            <Card.Text>{stocks}</Card.Text>
			            </Col>
		            </Row>

		        </Card.Body>
		    </Card>
		)
	}
}

// Check if the CourseCard component is getting the correct property types
ProductList.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		stocks: PropTypes.number.isRequired,
		price: PropTypes.number.isRequired
	})
}
